var app = angular.module('app', []);

app.controller('controller', function($scope) {
    $scope.count_right = 0;
    $scope.count_wrong = 0;

    var c;
    function add_problem(max) {
        max = max || 100;
        c = Math.floor(Math.random() * (max - 1)) + 1;
        $scope.a = Math.floor(Math.random() * (c - 2)) + 1;
        $scope.b = c - $scope.a;

        $scope.answer = undefined;
        $scope.right = undefined;
    }

    $scope.may_check = function() {
        return !!$scope.answer && $scope.right === undefined;
    }

    $scope.check_answer = function(problem) {
        $scope.right = c === parseInt($scope.answer);
        $scope.count_right += ($scope.right) ? 1 : 0;
        $scope.count_wrong += ($scope.right) ? 0 : 1;
    };

    $scope.new_problem = function() {
        add_problem();
    }

    add_problem();
})
